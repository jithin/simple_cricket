import random

from flask import Flask, request, redirect
from flask import render_template


app = Flask(__name__, static_url_path='/static')


@app.route("/")
def index():
    template_name = 'configuration.html'
    return render_template(template_name)


@app.route('/save_configuration', methods=["POST"])
def configure_game():
    toss_choices = ['head', 'tail']
    print 'aaaaa'
    team_strenght = request.form.get('team_strength', 5)
    overs = request.form.get('overs', 5)
    toss = request.form.get('', 'head')
    toss_result = random.choice(toss_choices)
    url = '/toss?team_members={}&overs={}&toss={}&toss_result={}'.format(team_strenght, overs, toss, toss_result)
    return redirect(url)


@app.route('/toss')
def toss():
    template_name = 'tossing.html'
    data = {'attributes': {}, 'url': ''}
    for key in request.args.keys():
        data['attributes'][key] = request.args.get(key)
    data['url'] = '/play?over={}&strength={}'.format(request.args.get('overs'), request.args.get('team_members'))
    return render_template(template_name, data=data)


@app.route('/play')
def letsplay():
    template_name = 'playgrount.html'
    return render_template(template_name)

if __name__ == "__main__":
    app.debug = True
    app.run()
